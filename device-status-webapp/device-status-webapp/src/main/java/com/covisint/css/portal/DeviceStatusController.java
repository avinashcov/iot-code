package com.covisint.css.portal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.covisint.iot.stream.MQTTStreamClient;

@Controller
public class DeviceStatusController {
	final static Logger logger = LoggerFactory.getLogger(DeviceStatusController.class);
    @Qualifier("mqttservice")
    @Autowired
    private MQTTStreamClient mqttClient;
    @MessageMapping("/getstatus")
    @SendTo("/topic/devicestatus" )
    public Device deviceStatus(DeviceMessage message) throws Exception {
    	logger.debug("Here in the message reader");
        Thread.sleep(3000); // simulated delay
        logger.debug("Done processing message"+message.getName());
        mqttClient.initializeMQTTConnection();
        return new Device("Hello, " + message.getName() + "!");
    }
    
    @MessageMapping("/getchartinfo")
    @SendTo("/topic/chartinfo")
    public Device getChartInfo(DeviceMessage message) throws Exception {
    	logger.debug("Here in the message reader");
        Thread.sleep(3000); // simulated delay
        logger.debug("Done processing message"+message.getName());
        return new Device("Hello, " + message.getName() + "!");
    }

}