package com.covisint.css.portal;

public class ChartData {
	private int devicesRunning;
	private int devicesStopped;
	private int totalDevices;
	private int devicesUnknown;
	public int getDevicesRunning() {
		return devicesRunning;
	}
	public void setDevicesRunning(int devicesRunning) {
		this.devicesRunning = devicesRunning;
	}
	public int getDevicesStopped() {
		return devicesStopped;
	}
	public void setDevicesStopped(int devicesStopped) {
		this.devicesStopped = devicesStopped;
	}
	public int getTotalDevices() {
		return totalDevices;
	}
	public void setTotalDevices(int totalDevices) {
		this.totalDevices = totalDevices;
	}
	public int getDevicesUnknown() {
		return devicesUnknown;
	}
	public void setDevicesUnknown(int devicesUnknown) {
		this.devicesUnknown = devicesUnknown;
	}

}
