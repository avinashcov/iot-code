package com.covisint.css.portal;

public class DeviceMessage {

    private String name;
    private String deviceId;
    private String command;
    private String publishToTopic;
    
    public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name=name;
    }
	public String getPublishToTopic() {
		return publishToTopic;
	}
	public void setPublishToTopic(String publishToTopic) {
		this.publishToTopic = publishToTopic;
	}
}
