
package com.covisint.iot.stream;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.covisint.css.portal.AjaxDeviceController;
import com.covisint.css.portal.Device;
import com.covisint.css.portal.utils.ExposablePropertyPlaceholderConfigurer;
import com.covisint.iot.stream.vo.StreamVO;
import com.google.gson.Gson;



@Component("ajaxmqttservice")
public class AjaxMQTTStreamClient implements MqttCallback{
	final static Logger logger = LoggerFactory.getLogger(AjaxMQTTStreamClient.class);
	// the MQTT client for Connecting to Covisint IoT Platform
	MqttClient client;
    @Qualifier("propertyConfigurer")
    @Autowired
    private ExposablePropertyPlaceholderConfigurer propertyConfigurer;
    

	//stores all the messages
	private final Map<String, LinkedBlockingQueue<Device>> messages = new HashMap<String,LinkedBlockingQueue<Device>>();	

    
    public Queue<Device> getMessages(String topic) {
		return messages.get(topic);
	}

	/** 
     * Initialize the MQTT client. Connect to the MQTT broker and subscribe to consumerTopic and alertConsumerTopic
     * 
     */
    public void initializeMQTTConnection(String topic)
    {
    	if(null==client || !client.isConnected()){
    		connectClient(topic);
    	}else{
    		subscribeAndInitializeTopic(topic);
    	}
    	
    }

	private void subscribeAndInitializeTopic(String topic) {
		try {
			client.subscribe(topic);
			if(null==messages.get(topic)){
				messages.put(topic, new LinkedBlockingQueue<Device>());
			}
		} catch (MqttException e) {
			logger.info("Subscribe failed for topic:"+topic);
		}
	}

	private void connectClient(String topic) {
		/**
    	 * Stream connectivity information. All information provided by Covisint Iot Stream service at the 
    	 * time of stream creation for this application.
    	 * ALL OF THESE VARIABLES SHOULD BE REPLACED WITH THE VALUES RETURNED WHEN CREATING YOUR COVISINT APPLICATION STREAM  
    	 */
    	// the host for the Covisint IoT MQTT broker
    	String connectionHost = propertyConfigurer.getProperty("mqtt.connectionHost");
    	// the port of the Covisint IoT MQTT broker
    	String connectionPort = propertyConfigurer.getProperty("mqtt.connectionPort");
    	// the username required for connecting to the Covisint IoT MQTT broker
    	String protocolSecurityUsername = propertyConfigurer.getProperty("mqtt.protocolSecurityUsername");
    	// the password required for connecting to the Covisint IoT MQTT broker
    	String protocolSecurityPassword = propertyConfigurer.getProperty("mqtt.protocolSecurityPassword");
    	// the clientID required for connecting to the Covisint IoT MQTT broker
    	String protocolSecurityClientId = propertyConfigurer.getProperty("mqtt.protocolSecurityClientId");
    	
    	/**
    	 * Topic information for application stream. All information provided by the Covisint IoT Stream service at the 
    	 * time of stream creation for this application.
    	 * ALL OF THESE VARIABLES SHOULD BE REPLACED WITH THE VALUES RETURNED WHEN CREATING YOUR COVISINT APPLICATION STREAM
    	 */
    	// topic to subscribe to. This topic has the events for the application stream
    	String consumerTopic = propertyConfigurer.getProperty("mqtt.consumerTopic");
    	// topic to subscribe to. This topic has the events that have caused and alert
    	String alertConsumerTopic = propertyConfigurer.getProperty("mqtt.alertConsumerTopic");
    	// topic to publish to. This topic is where you publish commands to be delivered to a device
    	String producerTopic = propertyConfigurer.getProperty("mqtt.producerTopic");
    	// The public key used to decrypt an event or alert's message JSON attribute from the consumerTopic
    	String consumerPublicKey = propertyConfigurer.getProperty("mqtt.consumerPublicKey");
    	// the private key used to encrypt a Command's message JSON attribute value.
    	String producerPrivateKey = propertyConfigurer.getProperty("mqtt.producerPrivateKey");
    	try
    	{  
    	  
	      client = new MqttClient("tcp://" + connectionHost + ":" + connectionPort , protocolSecurityClientId + new Random().nextInt());
	      MqttConnectOptions connectOptions =  new MqttConnectOptions();
	      connectOptions.setUserName(protocolSecurityUsername);
	      connectOptions.setPassword(protocolSecurityPassword.toCharArray());
	      connectOptions.setConnectionTimeout(Integer.parseInt(propertyConfigurer.getProperty("mqtt.connectionTimeOut")));
	      client.connect(connectOptions);
	      client.setCallback(this);

	      // subscribe to consumer topic in order to recieve events
	      client.subscribe(topic);
	      messages.put(topic, new LinkedBlockingQueue<Device>());
	      // subscribe to alter consumer topic in order to recieve events that caused an alert	      
	      client.subscribe(alertConsumerTopic);
    	} catch(Exception ex) {
    		ex.printStackTrace();
    	}
	}
    
    /**
     * Callback function when a Device event message is retrieved from subscribed topic. consumerTopic and alertConsumerTopic.
     * The message.getPayload will be in the format of a SendEvent message.
     * Read the SendEvent.encodingType of the message to determine if you should decode the SendEvent.message object. Can be base64 or none.
     * Use the consumerPublicKey to decrypt the message. Only decrypt if this Application Stream 
     * is configured to encrypt message.
     * 
     */
	public void messageArrived(String topic, MqttMessage message)
			throws Exception {
		try{
			String messageRecieved =new String(message.getPayload());
			if(null==messages.get(topic)){
				messages.put(topic, new LinkedBlockingQueue<Device>());
			}
			if (messages.get(topic).size() > 20) {
				messages.get(topic).remove();
			}
			Device deviceMessage = new Device(messageRecieved);
			messages.get(topic).add(deviceMessage);
		} catch (Exception e) {
			logger.error("Exception thrown",e);
		}

	}
	  
	  /**
	   * Construct a SendCommand message that will be published to the producerTopic. This message will ultimately 
	   * be delivered to the target device to process.
	   * 
	   * @param deviceId
	   * @param commandId
	   * @param commandArgs
	   */
	  public void publishCommand(String deviceId, String commandId, String commandArgs,String topic)
	  {
	    try {    	  
	      byte[] messageBytes = createStreamContent(deviceId, commandId);
	      MqttMessage message = new MqttMessage();
	      message.setPayload(messageBytes);
	      	// publish command to producer topic
		  client.publish(topic, message);
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	  }

	private byte[] createStreamContent(String deviceId, String commandId) {
//		StreamVO streamVO =  new StreamVO();
//	      streamVO.setMessageId(deviceId);
//	      streamVO.setMessage(commandId);
//	      streamVO.setCredentials(deviceId+":"+commandId);
		String deviceMessage = "\""+deviceId+"\""+" is running";
	    byte[] messageBytes = deviceMessage.getBytes();
		return messageBytes;
	}
	  
	  /**
	   * Based on the definition for the Application Stream.
	   * Encrypt the commandArgs using the producerPrivateKey and then encode.
	   * 
	   * @param commandArgs
	   * @return
	   */
	  private String encryptAndEncodeCommandArgs(String commandArgs)
	  {
		  return commandArgs;
	  }
	  
	  /**
	   * try to reconnect to the MQTT Broker with connectivity info and credentials  
	   */
	  public void connectionLost (Throwable cause) {}
	  
	  
	  public void deliveryComplete(IMqttDeliveryToken token) {
		  logger.info("Message delivered:"+token);
	  }
	  
	  private String findMessage(String regex, String expressionToParse){
		  Pattern pattern = Pattern.compile("\"(.*?)\"");
		  Matcher matcher = pattern.matcher(expressionToParse);
		  if (matcher.find())
		  {
		      return matcher.group(1);
		  }else{
			  return "not found";
		  }

	  
	  }
	  
	  public void stopSubscribe(String topic){
		  if(client.isConnected()){
			messages.get(topic).remove();
//				client.disconnect();
		   }
	  }
	  
}