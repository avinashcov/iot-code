package com.covisint.css.portal;

import org.testng.annotations.Test;

import com.google.gson.Gson;

public class TestJSON {
  @Test
  public void testHelloMessage() {
	  DeviceMessage hello = new DeviceMessage();
	  hello.setName("Avinash");
	  System.out.println("The json format:"+new Gson().toJson(hello));
	  
  }
  @Test
  public void testDeviceMessage() {
	  ChartData hello = new ChartData();
	  hello.setDevicesRunning(2);
	  hello.setDevicesStopped(5);
	  hello.setDevicesUnknown(7);
	  System.out.println("The json format:"+new Gson().toJson(hello));
	  
  }
}
