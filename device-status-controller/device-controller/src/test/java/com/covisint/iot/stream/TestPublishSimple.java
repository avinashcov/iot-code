package com.covisint.iot.stream;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

public class TestPublishSimple {
  public static void main(String[] args) {
	  ClassPathXmlApplicationContext cp= new ClassPathXmlApplicationContext("application-config.xml");
	  MQTTStreamClient sh = (MQTTStreamClient) cp.getBean("mqttservice");
	  sh.publishCommand("123456", "start", "godothis");;
  }
}
