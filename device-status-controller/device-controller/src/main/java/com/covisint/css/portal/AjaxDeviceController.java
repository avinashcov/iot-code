package com.covisint.css.portal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.covisint.iot.stream.AjaxMQTTStreamClient;
import com.google.gson.Gson;

@Controller
public class AjaxDeviceController{
	final static Logger logger = LoggerFactory.getLogger(AjaxDeviceController.class);
    @Qualifier("ajaxmqttservice")
    @Autowired
    private AjaxMQTTStreamClient mqttClient;

	@RequestMapping(value = "/publish", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void publish(HttpEntity<String> httpEntity) {
		logger.debug("Publishing message to device topic");
		String deviceMessage = httpEntity.getBody();
		DeviceMessage device = new Gson().fromJson(deviceMessage, DeviceMessage.class);
		logger.info("The request body has:"+deviceMessage);
		mqttClient.initializeMQTTConnection(device.getPublishToTopic());
		mqttClient.publishCommand(device.getName(), "starting", null,device.getPublishToTopic());
	}

	@RequestMapping(value = "/devicelog",method = RequestMethod.GET)	
	public @ResponseBody Device[] chatlog(@RequestParam("topic") String topic) {
		mqttClient.initializeMQTTConnection(topic);
		Queue<Device> deviceMessages = mqttClient.getMessages(topic);
		List<Device> devices = new ArrayList<Device>();

		if(null==deviceMessages || deviceMessages.isEmpty()){
			Device d = new Device("No message yet");
			Device[] array = new Device[1];
			array[0] = d;
			return array;
		}else{
			for (Iterator iterator = deviceMessages.iterator(); iterator.hasNext();) {
				Device device = (Device) iterator.next();
				logger.info("Devicelog:logging device message:"+device.getContent());
				devices.add(device);
			};
			Device[] array = new Device[devices.size()];
			devices.toArray(array);
			return (array);	
		}
	}
	
	@RequestMapping(value = "/stopLog", method = RequestMethod.POST)	
	@ResponseStatus(value = HttpStatus.OK)
	public void stopLogs(HttpEntity<String> httpEntity) {
		logger.info("stopping the logs");
		String deviceMessage = httpEntity.getBody();
		DeviceMessage device = new Gson().fromJson(deviceMessage, DeviceMessage.class);
		mqttClient.stopSubscribe(device.getPublishToTopic());
	}


}