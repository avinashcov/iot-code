package com.covisint.iot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.covisint.css.portal.utils.ExposablePropertyPlaceholderConfigurer;


@Controller
@RequestMapping("VIEW") //always map to �VIEW�
public class IOTController {
	@Qualifier("propertyConfigurer")
	@Autowired
	private ExposablePropertyPlaceholderConfigurer propertyConfigurer;
	@RenderMapping
    public String helloWorld(Model model) {
        model.addAttribute("message", "Hello World!");
        String clientId = propertyConfigurer.getProperty("client.id.for.css-i1.portal.covapp.io");
        System.out.println("ClientID:>>>>>>"+clientId);
        return "helloWorld";
    }
}
